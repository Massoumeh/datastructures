package day03wordsfreq;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

public class Day03WordsFreq {

    static Map<String, Integer> map = new HashMap<>();

    public static void main(String[] args) throws FileNotFoundException, Exception {
        //wordFrequency();
        long startTime = System.currentTimeMillis();
        readDataFromFile();
        long endTime = System.currentTimeMillis();
        double duration = (endTime - startTime) / 1000.0;
        startTime = System.currentTimeMillis();
        System.out.println("File took " + duration + " seconds to load");
        System.out.println("");
        System.out.println("God - Frequency = " + wordFreq.get("god"));
        System.out.println("lord - Frequency = " + wordFreq.get("lord"));
        System.out.println("called - Frequency = " + wordFreq.get("called"));
        System.out.println("blessing - Frequency = " + wordFreq.get("blessing"));
        System.out.println("");
        sortTopTen();
        endTime = System.currentTimeMillis();
        duration = (endTime - startTime) / 1000.0;
        System.out.println("Operations took " + duration + " seconds to load");


    }
    static HashMap<String, Integer> wordFreq = new HashMap<String, Integer>();

    static void readDataFromFile() {    //  Count is wrong? also whitespace is included
        try ( Scanner fileInput = new Scanner(new File("Bible.txt")).useDelimiter("[ \\s\\t\\n\\r\\\\.,:;0123456789-]")) {
            while (fileInput.hasNext()) {
                String word = fileInput.next();
                word = word.toLowerCase(); // case-insensitive search
                if (word.length() == 0) continue;
                if (wordFreq.containsKey(word)) {
                    int num = wordFreq.get(word);
                    num++;
                    wordFreq.put(word, num);
                } else {
                    wordFreq.put(word, 1);
                }
            }
            // DO NOT DO THAT: try-with-resources will already close the input: fileInput.close();
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    class Pair {
        String value;
        int key;
    }
    
    static void sortTopTen() {
        long start = System.currentTimeMillis();
        // create a list of pairs of values from Key-Value paris stored in HashMap
        ArrayList list = new ArrayList(wordFreq.entrySet());
        // sort the pair objects by value (frequency)
        Collections.sort(list,
                (Map.Entry o1, Map.Entry o2) -> ((Comparable) o2.getValue()).compareTo(o1.getValue()));
        //
        System.out.println("TOP TEN WORDS:");
        for (int i = 0; i < 10; i++) {
            System.out.println(list.get(i));
        }
        long time = System.currentTimeMillis() - start;
        System.out.println("TOP TEN WORDS Operation took " + time + "ms");
    }

    
/*
    

    public static void wordFrequency() throws FileNotFoundException {
        String word = null;

        Scanner scanner = new Scanner(new File("input.txt")).useDelimiter("[ \\t\\n\\r\\\\.,:;-]");

        while (scanner.hasNext()) {
            word = scanner.next();
            if (map.containsKey(word)) {

                map.put(word, map.get(word) + 1);

            } else {
                map.put(word, 1);
            }
        }

        System.out.println("Number of God inside the file is: " + map.get("God"));
        System.out.println("Number of LORD inside the file is: " + map.get("LORD"));
        System.out.println("Number of called inside the file is: " + map.get("called"));
        System.out.println("Number of blessing inside the file is: " + map.get("blessing"));

        System.out.println("=================");
        Collection<Integer> values = map.values();
        ArrayList<Integer> list = new ArrayList<Integer>(values);
        Collections.sort(list, Collections.reverseOrder());
        for (int i = 0; i < 10; i++) {
            System.out.println(list.get(i) + word); // ?
        }
    }
*/
}


