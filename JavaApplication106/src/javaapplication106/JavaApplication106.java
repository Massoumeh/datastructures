/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication106;

/**
 *
 * @author Mmaha
 */
public class JavaApplication106 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double sumOfRow = 0;
        double[][] matrix = new double[3][4];
        java.util.Scanner input = new java.util.Scanner(System.in); //Scanner
        System.out.println("Enter a 3 by 4 matrix row by row: ");
        //Prompt user to enter matrix numbers
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[0].length; col++) {
                matrix[row][col] = input.nextDouble();
            }
        }
        double[] sumOfCol =new double[matrix[0].length];  
        for (int i = 0; i < matrix.length; i++) { //row
            for (int j = 0; j < matrix[i].length; j++) { //column
                sumOfRow += matrix[i][j];
                sumOfCol[j] += matrix[i][j];
            }
            System.out.println("Sum of the elements at row " + row + " is: " + sumOfRow);
        }
        System.out.println("Sum of the elements at column " + col + " is: " + sumOfCol);
    }
    
}
