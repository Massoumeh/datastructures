package day02carssorted;

import com.sun.media.sound.InvalidDataException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

class Car implements Comparable<Car> {

    String makeModel;
    double engineSizeL;
    int prodYear;

    public Car(String makeModel, double engineSizeL, int prodYear) {
        this.makeModel = makeModel;
        this.engineSizeL = engineSizeL;
        this.prodYear = prodYear;
    }

    @Override
    public String toString() {
        return "Car{" + "makeModel=" + makeModel + ", engineSizeL=" + engineSizeL + ", prodYear=" + prodYear + '}';
    }

    @Override
    public int compareTo(Car car) {
        return this.makeModel.compareTo(car.makeModel);
    }

    static final Comparator<Car> comparatorByEngineSizeL = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            return Double.compare(car1.engineSizeL, car2.engineSizeL);
        }
    };

    static final Comparator<Car> comparatorByProdYear = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            return Double.compare(car1.prodYear, car2.prodYear);
        }
    };

    static final Comparator<Car> comparatorByProdYearAndMakeModel = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            int ret = Double.compare(car1.prodYear, car2.prodYear);
            if (ret == 0) {
                ret = car1.makeModel.compareTo(car2.makeModel);
            }
          return ret;  
        }
    };
}

public class Day02CarsSorted {

    static ArrayList<Car> parking = new ArrayList<>();
    
    static final String FILE_NAME = "cars.txt";
    
    static void readDataFromFile() {
        try (Scanner fileInput = new Scanner(new File(FILE_NAME))) {
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();
                    String[] data = line.split(";"); // may have wrong number of fields
                    if (data.length != 3) {
                        System.out.println("Data error: invalid number of items in line.");
                        continue;
                    }
                    String makeModel = data[0];
                    double engineSizeL = Double.parseDouble(data[1]); // may cause exception
                    int prodYear = Integer.parseInt(data[2]);
                    Car car = new Car(makeModel, engineSizeL,prodYear);
                    parking.add(car);
                } catch (NumberFormatException ex) {
                    System.out.println("Data error: " + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        readDataFromFile();
        parking.add(new Car("Ford", 2.5, 2015));
        parking.add(new Car("Volvo", 3.1, 2018));
        parking.add(new Car("Honda", 2.5, 2009));
        parking.add(new Car("Toyota", 4.5, 2020));
        parking.add(new Car("BMW", 3.8, 2015));
        parking.add(new Car("Mazda", 2.9, 2018));

        System.out.println("======== ORIGINAL INITIAL ORDER");
        for (Car car : parking) {
            System.out.println(car);
        }

        Collections.sort(parking);
        System.out.println("======== SORTED BY makeModel");
        for (Car car : parking) {
            System.out.println(car);
        }

        Collections.sort(parking, Car.comparatorByEngineSizeL);
        System.out.println("======== SORTED BY EngineSizeL");
        for (Car car : parking) {
            System.out.println(car);
        }

        Collections.sort(parking, Car.comparatorByProdYear);
        System.out.println("======== SORTED BY ProdYear");
        for (Car car : parking) {
            System.out.println(car);
        }
        
        Collections.sort(parking, Car.comparatorByProdYearAndMakeModel);
        System.out.println("======== SORTED BY ProdYear And MakeModel");
        for (Car car : parking) {
            System.out.println(car);
        }
    }

}
