/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication105;

import java.util.Random;

/**
 *
 * @author Mmaha
 */
public class JavaApplication105 {

     public static void main(String[] args) {
        int[][] nums = new int[9][9];

        handleArray(nums);

        printArray(nums);
    }

    private static int getSumOfRow(int[][] array, int rowNum) {
        int sumOfRows = 0;

        for(int i = 0; i < array[rowNum].length; i++)
            sumOfRows += array[rowNum][i];

        return sumOfRows;
    }

    private static int getColumnSum(int[][] array, int columnNum) {
        int sumOfColumn = 0;

        for(int i = 0; i < array.length; i++)
            sumOfColumn += array[i][columnNum];

        return sumOfColumn;
    }

    private static void handleArray(int[][] array) {
        Random randomGenerator = new Random();

        for (int i=0; i < array.length; i++) {
            for (int j=0; j < array[i].length; j++) {
                int random = randomGenerator.nextInt(100);
                array[i][j] = random;
            }
        }
    }

    private static void printArray(int[][] array) {
        System.out.printf("       ");
        for(int i = 0; i < array.length; i++)
            System.out.printf("%4d ", i);
        System.out.println("\n   __________________________________________________");

        for(int i = 0; i < array.length; i++) {
            System.out.printf("%4d | ", i);

            for(int j = 0; j < array[i].length; j++)
                System.out.printf("%4d ", array[i][j]);

            System.out.printf("| Sum = %4d", getSumOfRow(array, i));

            System.out.println();
        }

        System.out.println("   __________________________________________________");

        handleSumOfColumnPrint(array);
    }

    private static void handleSumOfColumnPrint(int[][] array) {
        System.out.printf("       ");

        for(int i = 0; i < array[0].length; i++)
            System.out.printf("%4d ", getColumnSum(array, i));
    }

}
