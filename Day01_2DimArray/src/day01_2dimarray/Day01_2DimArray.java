package day01_2dimarray;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Day01_2DimArray {

    static Scanner input = new Scanner(System.in);
    
    static boolean isPrime(int inputNumber) {
        if (inputNumber <= 1) {
            return false;
        }
        boolean isItPrime = true;
        for (int i = 2; i <= inputNumber / 2; i++) {
            if ((inputNumber % i) == 0) {
                return false; // not a prime number
            }
        }
        return true; // not divisible therefore a prime number
    }
    
    static void findPrimeNumberPairsUnique(int[][] array) {
        // marker one moves
        for (int row1 = 0; row1 < array.length; row1++) {
            for (int col1 = 0; col1 < array[row1].length; col1++) {
                // marker two moves
                for (int row2 = row1; row2 < array.length; row2++) {
                    for (int col2 = (row1==row2 ? col1 : 0); col2 < array[row2].length; col2++) {
                        if (row1 == row2 && col1 == col2) {
                            continue;
                        }
                        int sum = array[row1][col1] + array[row2][col2];
                        if (isPrime(sum)) {
                            System.out.printf("[%d,%d]:%d + [%d,%d]:%d = %d which is a prime number\n",
                                    row1, col1, array[row1][col1], row2, col2, array[row2][col2], sum);
                        }
                    }
                }
            }
        }
    }    
    
    
    public static void main(String[] args) {
        System.out.println("Enter the Row of an array");
        int Row = input.nextInt();
        System.out.println("Enter the Column of an array");
        int Column = input.nextInt();
        int[][] twoDimArray = new int[Row][Column];
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[i].length; j++) {
                //-min + (int) (Math.random() * ((max - (-min)) + 1));
                twoDimArray[i][j] = -99 + (int) (Math.random() * ((99 - (-99)) + 1));

                System.out.print((j == 0 ? "" : " , ") + twoDimArray[i][j]);
            }
            System.out.println();
        }
        System.out.println("________________");
        int sumt = 0;

        for (int r = 0; r < twoDimArray.length; r++) {

            for (int c = 0; c < twoDimArray[r].length; c++) {
                sumt += twoDimArray[r][c];
            }
        }
        System.out.printf("Sum of all items is: %d\n", sumt);
        System.out.println("________________");
        
        int sum = 0;
        int rowSize = twoDimArray.length;
        int columnSize = twoDimArray[0].length;
        System.out.println("rows = " + rowSize + " cols =" + columnSize);
        int[] colSum = new int[twoDimArray[0].length];
        for (int i = 0; i < twoDimArray.length; i++) {
            sum = 0;
            for (int j = 0; j < twoDimArray[0].length; j++) {
                sum += twoDimArray[i][j];
                colSum[j] += twoDimArray[i][j];
            }
            System.out.println("The sum of rows = " + sum);
            System.out.println("________________");
        }
        for (int k = 0; k < colSum.length; k++) {
            System.out.println("The sum of columns = " + colSum[k]);
        }
        System.out.println("________________");
        //sum
        //count = Row * Column
        //mean = sum / count
        
        float s2 = 0;
        int count = Row * Column;
        float mean = (float) sumt/ count;
        for (int ROw = 0; ROw < twoDimArray.length; ROw++) {
            for (int Col = 0; Col < twoDimArray[ROw].length; Col++) {   
                s2 +=(float) Math.pow(twoDimArray[ROw][Col] - mean,2)/ (count-1);
            }
        }
        System.out.printf("Total number in the array is: %d\n" ,count);
        System.out.printf("Average of all numbers in the array is: %f\n" ,mean);
        System.out.printf("Standard deviation of all numbers in the array is: %f\n" ,Math.sqrt(s2));

      System.out.println("________________");
       // findPrimeNumberPairs(array);
        findPrimeNumberPairsUnique(twoDimArray);
    }
    
}
