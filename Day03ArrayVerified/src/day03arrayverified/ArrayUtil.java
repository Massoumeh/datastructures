
package day03arrayverified;

public class ArrayUtil {
    
    static int sumAll(int[][] array) {
        int sum = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
            }
        }
        return sum;
    }
    
    static double standardDeviation(int[][] array) {
        double sum = 0.0, sumOfSquaredDiffs = 0.0;
        int itemsCount = array.length * array[0].length; // if rectangular array

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                sum += array[i][j];
            }
        }
        double average = sum / itemsCount;
        for (int row = 0; row < array.length; row++) {
            for (int col = 0; col < array[row].length; col++) {
                sumOfSquaredDiffs += Math.pow(array[row][col] - average, 2);
            }
        }

        return Math.sqrt(sumOfSquaredDiffs / itemsCount);
    }
}
