package day03arrayverified;

public class Day03ArrayVerified {

    public static void main(String[] args) {
        int[][] testData2Dalpha = {{5, -1, 34,}, {17, 2, 5, 2}, {22, 11, 3}};
        int[][] testData2Dbeta = {{5, -1, 34,}, {17, 2, 5}, {22, 11, 3}};
        ArrayUtil au = new ArrayUtil();
        { // test 1
            int sum = au.sumAll(testData2Dalpha);
            int expected = 100;
            System.out.println("Sum is : " + sum + ", expected was: 100 ");
            if (sum != expected) {
                System.out.println("ERROR: Sum is wrong");
            }
        }

        { // test 2
            double stdDev = au.standardDeviation(testData2Dalpha);
            double expected = 10.785220797662d;
            System.out.printf("StdDev is %f, expected was: %f\n", stdDev, expected);
            if (stdDev != expected) {
                System.out.println("ERROR: StdDev is wrong");
            }
        }
    }
}
