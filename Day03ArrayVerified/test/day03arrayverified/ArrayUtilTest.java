/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03arrayverified;


import org.junit.After;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

public class ArrayUtilTest {
  
    public ArrayUtilTest() {
    }
 
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     @Test
    public void testSumAllJagged() {
        System.out.println("sumAll");
        int[][] array = {{5, -1, 34,}, {17, 2, 5, 5}, {22, 11, 3}};
        ArrayUtil instance = new ArrayUtil();
        int expResult = 103;
        int result = instance.sumAll(array);
        assertEquals("sumAll failed on jagged array", expResult, result);
    }

    @Test
    public void testSumAllRectangular() {
        System.out.println("sumAll");
        int[][] array = {{5, -1, 34,}, {17, 2, 5}, {22, 11, 3}};
        ArrayUtil instance = new ArrayUtil();
        int expResult = 98;
        int result = instance.sumAll(array);
        assertEquals("sumAll failed on rectangular array", expResult, result);
    }


    /**
     * Test of standardDeviation method, of class ArrayUtil.
     */
    @Test
    public void testStandardDeviationJagged() {
        System.out.println("standardDeviation");
        int[][] array = {{5, -1, 34,}, {17, 2, 5, 5}, {22, 11, 3}};
        double expResult = 10.383159442097;
        double result = ArrayUtil.standardDeviation(array);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    @Test
    public void testStandardDeviationRectangular() {
        System.out.println("standardDeviation");
        int[][] array = {{5, -1, 34,}, {17, 2, 5}, {22, 11, 3}};
        double expResult = 10.785220797662;
        double result = ArrayUtil.standardDeviation(array);
        assertEquals(expResult, result, 0.0);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
