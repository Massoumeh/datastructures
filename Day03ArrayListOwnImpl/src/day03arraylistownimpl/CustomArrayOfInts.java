package day03arraylistownimpl;

public class CustomArrayOfInts {

    private int[] data = new int[1]; // only grows by doubling size, never shrinks
    private int size = 0; // how many items do you really have

    public int size() {
        return size;
    }

    private void growIfNeede() {
        int[] newData = new int[data.length * 2];
        //copy data from old array to new one
        for (int i = 0; i < data.length; i++) {
            newData[i] = data[i];

        }
        data = newData;
    }
    
    private void shrinkIfNeed(){
        int[] newData = new int[data.length/2];
        for (int i = 0; i < newData.length; i++) {
            newData[i] = data[i];
        }
        data = newData;
    }

    public void add(int value) {
        if (data.length == size) {//must grow storage first
            growIfNeede();

        }
        data[size] = value;
        size++;
    }

    public void deleteByIndex(int index) {//data.remove(index);

        for (int i = index; i < size - 1; i++) {
            
            data[i] = data[i + 1];
            System.out.println(i + ":" + toString());
        }
        size--;
    }

    public void deleteByValue(int value) { // delete first value matching
        for (int i = 0; i < size - 1; i++) {
            if (value == data[i]) {
                for (int j = i; j < size - 1; j++) {
                    data[j] = data[j + 1];
                    System.out.println(i + ":" + toString());
                }
            }

        }
        size--;
    } 

    public void insertValueAtIndex(int value, int index) {
        if (data.length == size) {
            growIfNeede();
        }
        if (index > size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        for (int i = size - 1; i >= index; i--) {
            data[i + 1] = data[i];
            System.out.println(i + ": " + toString());
        }
        data[index] = value;
        size++;
    }

    public void clear() {
        size = 0;
    }

    public int get(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return data[index];
    }

    public int[] getSlice(int startIdx, int length) {//???
        int []slice = new int[length];
        int endIdx = startIdx + length-1;
        if (startIdx < 0 || endIdx > size-1 || slice.length > data.length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        for (int i = startIdx; i <= endIdx; i++) {
            slice[i-startIdx]=data[i];
            System.out.println(slice[i] + i == 0? "":"," + toString());
        }
        data = slice;
        return data;
    }
    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size; i++) {//if using data.length, you have to 
            //declaire the ziros after the size of array for test unit
            sb.append(i == 0 ? "" : ",");
            sb.append(data[i]);
        }
        sb.append("]");

        return sb.toString();
    } // returns String similar to: [3, 5, 6, -23]
}
