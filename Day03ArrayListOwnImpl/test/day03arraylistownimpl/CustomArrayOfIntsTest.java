package day03arraylistownimpl;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CustomArrayOfIntsTest {

    @Test
    public void simpleAddAndGett() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);
        assertEquals(5, instance.size());
        assertEquals(13, instance.get(4));
        assertEquals(11, instance.get(2));
        assertEquals(9, instance.get(1));
        assertEquals(3, instance.get(0));
        assertEquals(-4, instance.get(3));

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void InvalidMinusOne() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.get(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void InvalidTooHigh() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);
        instance.get(5);
    }

    @Test
    public void insertAtTwo() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);//2
        instance.add(-4);
        instance.add(13);
        instance.insertValueAtIndex(88, 2);
        assertEquals("[3,9,88,11,-4,13]", instance.toString());
    }
    @Test
    public void insertAtThree() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);
        instance.add(-4);//3
        instance.add(13);
        instance.insertValueAtIndex(88, 3);
        assertEquals("[3,9,11,88,-4,13]", instance.toString());
    }
        @Test
    public void insertAtFive() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);
        instance.add(18);//5
        instance.insertValueAtIndex(88, 5);
        assertEquals("[3,9,11,-4,13,88,18]", instance.toString());
    }
    @Test
     public void insertAtZiro() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);//0
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);
        instance.add(18);
        instance.insertValueAtIndex(88, 0);
        assertEquals("[88,3,9,11,-4,13,18]", instance.toString());
    }
     @Test
     public void deleteByIndextwo(){
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);//2
        instance.add(-4);
        instance.add(13);
        instance.add(18);
        instance.deleteByIndex(2);
        assertEquals("[3,9,-4,13,18]", instance.toString());
     }
     @Test
     public void deleteByIndexziro(){
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);//0
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);
        instance.add(18);
        instance.deleteByIndex(0);
        assertEquals("[9,11,-4,13,18]", instance.toString());
     }
     @Test
     public void deleteByIndexFive(){
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);
        instance.add(18);//5
        instance.deleteByIndex(5);
        assertEquals("[3,9,11,-4,13]", instance.toString());
     }
     @Test
     public void deleteByValue(){
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);
        instance.add(18);//5
        instance.deleteByValue(11);
        assertEquals("[3,9,-4,13,18]", instance.toString());
     }
     @Test
     public void deleteByLastValue(){
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);
        instance.add(18);
        instance.deleteByValue(18);
        assertEquals("[3,9,11,-4,13]", instance.toString());
     }
    @Test
    public void deleteByFirstValue() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);
        instance.add(18);
        instance.deleteByValue(3);
        assertEquals("[9,11,-4,13,18]", instance.toString());
    }
     @Test
    public void getSliceSample() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);//start
        instance.add(-4);
        instance.add(13);//end
        instance.add(18);
        instance.getSlice(2, 3);
        assertEquals("[11,-4,13]", instance.toString());
    }
       /*  @Test
    public void getSliceFirst() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);//start
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);//end
        instance.add(18);
        instance.getSlice(0, 5);
        assertEquals("[3,9,11,-4,13]", instance.toString());
    }*/
    
    @Test(expected = IndexOutOfBoundsException.class)
    public void InvalidSliceMinusOne() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        instance.getSlice(-1, 3);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void InvalidSliceTooHigh() {
        CustomArrayOfInts instance = new CustomArrayOfInts();
        assertEquals(0, instance.size());
        instance.add(3);
        instance.add(9);
        instance.add(11);
        instance.add(-4);
        instance.add(13);
        instance.getSlice(3, 3);
    }
}