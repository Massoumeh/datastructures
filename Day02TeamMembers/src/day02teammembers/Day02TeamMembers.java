package day02teammembers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Day02TeamMembers {

    public static void main(String[] args) throws IOException {
        String filePath = "teams.txt";
        HashMap<String, ArrayList<String>> playersByTeams = new HashMap<>();
        ArrayList<String> teamList = new ArrayList<String>();

        String line;
        String value;
        String key;
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split(":", 2);
            if (parts.length >= 2) {
                value = parts[0];
                teamList.add(value);
                key = parts[1];
                String[] names = key.split(",");
                for (String name : names) {
                    playersByTeams.put(name, teamList);
                }

            } else {
                System.out.println("ignoring line: " + line);
            }

            //Example output:
            //Jerry plays in: Green Grass Team, Terrific Team Today, Other Best Team, Yet Another Super-Team
            //for (String name : names) {
            // }
        }  System.out.println(playersByTeams);
        reader.close();
      
    }

}
