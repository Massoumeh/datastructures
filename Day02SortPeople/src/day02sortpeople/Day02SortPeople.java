package day02sortpeople;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Person implements Comparable<Person> {

    String name;
    int age;
    double heightMeters;

    public Person(String name, int age, double heightMeters) {
        this.name = name;
        this.age = age;
        this.heightMeters = heightMeters;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", age=" + age + ", heightMeters=" + heightMeters + '}';
    }
    @Override
    public int compareTo(Person person) {
        return this.name.compareTo(person.name);
    }
     static final Comparator<Person> comparatorByAge = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            return p1.age - p2.age;
        }
    };

    static final Comparator<Person> comparatorByAgeLambdaExample = (Person p1, Person p2) -> p1.age - p2.age;

    static final Comparator<Person> comparatorByHeight = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            // GOOD ALTERNATIVE: return Double.compare(p1.heightMeters, p2.heightMeters);
            // System.out.print("h ");
            if (p1.heightMeters == p2.heightMeters) {
                return 0;
            }
            if (p1.heightMeters > p2.heightMeters) {
                return 1;
            } else {
                return -1;
            }
        }
    };

}

public class Day02SortPeople {

    public static void main(String[] args) {
        List<Person> peopleList = new ArrayList<Person>();
        peopleList.add(new Person("John", 40, 1.82));
        peopleList.add(new Person("Jake", 19, 1.65));
        peopleList.add(new Person("Cathy", 12, 1.54));
        peopleList.add(new Person("David", 63, 1.76));
        peopleList.add(new Person("Bob", 14, 1.83));
        
        
        System.out.println("======== ORIGINAL INITIAL ORDER");
        for (Person p : peopleList) {
            System.out.println(p.toString());
        }
        Collections.sort(peopleList);
        System.out.println("======== SORTED BY NAME");
        for (Person p : peopleList) {
            System.out.println(p);
        }
        //Collections.sort(peopleList);
        
        Collections.sort(peopleList, Person.comparatorByAge);
        System.out.println("======== SORTED BY AGE");
        for (Person p : peopleList) {
            System.out.println(p);
        }
        //
        Collections.sort(peopleList, Person.comparatorByHeight);
        System.out.println("======== SORTED BY HEIGHT");
        for (Person p : peopleList) {
            System.out.println(p);
        }
        //
        Collections.sort(peopleList, Person.comparatorByAgeLambdaExample);
        System.out.println("======== SORTED BY AGE USING LAMBDA");
        for (Person p : peopleList) {
            System.out.println(p);
        }
        //
        Collections.sort(peopleList, (Person p1, Person p2) -> p1.age - p2.age );
        System.out.println("======== SORTED BY AGE USING LAMBDA AS PARAMETER");
        for (Person p : peopleList) {
            System.out.println(p);
        }
    }

}
