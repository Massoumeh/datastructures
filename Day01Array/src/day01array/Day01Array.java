
package day01array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;


public class Day01Array {
    static Scanner input = new Scanner(System.in);
    static Random random = new Random();
    
    static boolean isPrime(int inputNumber) {
        if (inputNumber <= 1) {
            return false;
        }
        boolean isItPrime = true;
        for (int i = 2; i <= inputNumber / 2; i++) {
            if ((inputNumber % i) == 0) {
                return false; // not a prime number
            }
        }
        return true; // not divisible therefore a prime number
    }
    
    public static void main(String[] args) {
        
        System.out.print("How big an array to use? ");
        int size;
        try {
            size = input.nextInt();
        } catch (InputMismatchException ex) {
            System.out.println("Error: you must provide an integer number");
            return; // end program
        }
        if (size < 1) {
            System.out.println("Error: size must be 1 or greater");
            return; // end program
        }
        int data[] = new int[size];
        for (int i = 0; i < size; i++) {
            data[i] = random.nextInt(100) + 1; // 1-100
        }
        System.out.print("Values generated: ");
        for (int i = 0; i < size; i++) {
            System.out.print((i == 0 ? "" : ",") + data[i]);
        }
        System.out.println();
        // find prime numbers and put them into ArrayList
        ArrayList<Integer> primesOnlyList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            if (isPrime(data[i])) {
                primesOnlyList.add(data[i]);
            }
        }
        // display comma-separated from ArrayList
        System.out.print("Values that are prime numbers: ");
        for (int i = 0; i < primesOnlyList.size(); i++) {
            System.out.print((i == 0 ? "" : ",") + primesOnlyList.get(i));
        }
        System.out.println();
    }
    
}
